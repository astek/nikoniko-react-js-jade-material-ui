/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes, Component } from 'react';
import styles from './ContactPage.css';
import withStyles from '../../decorators/withStyles';
import FlatButton from 'material-ui/lib/flat-button';


@withStyles(styles) class ContactPage extends React.Component {

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired
  };

  render() {
    const title = 'Contact Us';
    this.context.onSetTitle(title);
    return (
      <div className="ContactPage">
        <div className="ContactPage-container">
          <h1>{title}</h1>

          <div>
            <FlatButton label="Default"/>
            <FlatButton label="Primary" primary={true}/>
            <FlatButton label="Secondary" secondary={true}/>
            <FlatButton label="Disabled" disabled={true}/>
          </div>
          <p>...</p>
        </div>
      </div>
    );
  }

}

export default ContactPage;
