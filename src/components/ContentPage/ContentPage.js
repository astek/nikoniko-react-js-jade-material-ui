/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes, Component } from 'react';
import styles from './ContentPage.css';
import withStyles from '../../decorators/withStyles';
import RadioButton from 'material-ui/lib/radio-button';
import RadioButtonGroup from 'material-ui/lib/radio-button-group';
import Card from 'material-ui/lib/card/card';
import CardTitle from 'material-ui/lib/card/card-title';
import RaisedButton from 'material-ui/lib/raised-button';
import TextField from 'material-ui/lib/text-field';
import DatePicker from 'material-ui/lib/date-picker/date-picker';

@withStyles(styles) class ContentPage extends Component {

  static propTypes = {
    path: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    title: PropTypes.string,
  };

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
  };

  render() {
    this.context.onSetTitle(this.props.title);

    const styles = {
      block: {
        maxWidth: 250,
      },
      radioButton: {
        marginBottom: 16,
      },
      card: {
        padding: 10,
        margin: 40,
      }
    };

    return (
      <div className="ContentPage">
        <div className="ContentPage-container">
          {
            this.props.path === '/' ? null : <h1>{this.props.title}</h1>
          }
          <div dangerouslySetInnerHTML={{__html: this.props.content || ''}}/>
          <Card style={styles.card}>
            <CardTitle title="Vote"/>

            <div style={styles.block}>
              <TextField
                hintText="Username"
                /><br/>
              <DatePicker hintText="Date"/><br/>
              <RadioButtonGroup name="shipSpeed" defaultSelected="not_light">
                <RadioButton
                  value="MAD"
                  label="MAD"
                  style={styles.radioButton}
                  />
                <RadioButton
                  value="SAD"
                  label="SAD"
                  style={styles.radioButton}
                  />
                <RadioButton
                  value="GLAD"
                  label="GLAD"
                  style={styles.radioButton}
                  />
              </RadioButtonGroup>
              <RaisedButton label="Send" secondary={true}/>
            </div>
          </Card>
        </div>
      </div>
    );
  }

}

export default ContentPage;
